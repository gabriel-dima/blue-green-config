# Blue Green Persist Configuration 

This repository contains the blue-green PersistConfig settings needed to perform the switching of ENIs between active and Inactive Clusters. 

More documentation of the release tooling can be found here at: 

    - https://bitbucket.org/globalreachrad/blue-green-lambda/src/master/
    - https://cloudessa.atlassian.net/wiki/spaces/DKB/pages/551747585/Odyssys+Blue+Green+infrastructure

